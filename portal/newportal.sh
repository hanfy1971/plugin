#!/bin/sh

echo "> downloading & installing portals files  Please Wait ..."
sleep 3

wget --show-progress -qO /etc/enigma2/MultiStalkerPro.json "https://gitlab.com/hanfy1971/plugin/-/raw/main/portal/NewMultiStalkerPro.json"
download=$?
echo ''
if [ $download -eq 0 ]; then
echo "> installation of portals files  finished"
sleep 3

echo "> Setup the plugin..."
# Configure ajpanel_settings
touch "$temp_dir/temp_file"
cat <<EOF > "$temp_dir/temp_file"
config.plugins.MultiStalkerPro.adult=True
config.plugins.MultiStalkerPro.color=Moonlit Asteroid
config.plugins.MultiStalkerPro.extplayer_subtitle_font_color=#FFFF00
config.plugins.MultiStalkerPro.extplayer_subtitle_pos=70
config.plugins.MultiStalkerPro.host=http://ultraflix.uk:8080/c/
config.plugins.MultiStalkerPro.mac=00:1A:79:51:d6:1b
config.plugins.MultiStalkerPro.mainmenu=True
config.plugins.MultiStalkerPro.portalNB=4
config.plugins.MultiStalkerPro.PvrServiceType=5002
config.plugins.MultiStalkerPro.subDL=AR
config.plugins.MultiStalkerPro.tmdb_key=d48cfffeaefab8031ebc14b8dbabf146
config.plugins.MultiStalkerPro.tmdbL=ar-AR
EOF

# Update Enigma2 settings
sed -i "/MultiStalkerPro/d" /etc/enigma2/settings
grep "config.plugins.MultiStalkerPro.*" "$temp_dir/temp_file" >> /etc/enigma2/settings
rm -rf "$temp_dir/temp_file" >/dev/null 2>&1

sleep 2
echo "> Setup done..., Please wait enigma2 restarting..."
sleep 1
echo "> Uploaded By TAREK"

# Restart Enigma2 service or kill enigma2 based on the system
if [ -f /etc/apt/apt.conf ]; then
    sleep 2
    systemctl restart enigma2
else
    sleep 2
    killall -9 enigma2
fi
else
echo "> $plugin-$version package installation failed"
sleep 3
fi
    
