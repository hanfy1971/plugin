#!/bin/sh

declare -A files=(

["/usr/lib/enigma2/python/Plugins/Extensions/EPGGrabber/providers/beinsportiet5.py"]="https://github.com/Saiedf/EpgGrabber/raw/main/beinsportiet5.py"
 
["/usr/lib/enigma2/python/Plugins/Extensions/EPGGrabber/providers/uaeariet5.py"]="https://github.com/Saiedf/EpgGrabber/raw/main/uaeariet5.py"

["/usr/lib/enigma2/python/Plugins/Extensions/EPGGrabber/providers/uaeeniet5.py"]="https://github.com/Saiedf/EpgGrabber/raw/main/uaeeniet5.py"

["/usr/lib/enigma2/python/Plugins/Extensions/EPGGrabber/providers/elcin.py"]="https://github.com/Saiedf/EpgGrabber/raw/main/elcin.py"

["/etc/epgimport/ziko_config/beinsportiet5.channels.xml"]="https://github.com/Saiedf/EpgGrabber/raw/main/beinsportiet5.channels.xml"
    ["/etc/epgimport/ziko_config/uaeariet5.channels.xml"]="https://github.com/Saiedf/EpgGrabber/raw/main/uaeariet5.channels.xml"
    ["/etc/epgimport/ziko_config/uaeeniet5.channels.xml"]="https://github.com/Saiedf/EpgGrabber/raw/main/uaeeniet5.channels.xml"
    ["/etc/epgimport/custom.sources.xml"]="https://github.com/Saiedf/EpgGrabber/raw/main/custom.sources.xml"

["/usr/lib/enigma2/python/Plugins/Extensions/EPGGrabber/api/bouquets.json"]="https://github.com/Saiedf/EpgGrabber/raw/main/bouquets.json"
    ["/usr/lib/enigma2/python/Plugins/Extensions/EPGGrabber/api/providers.json"]="https://github.com/Saiedf/EpgGrabber/raw/main/providers.json"
)

for file in "${!files[@]}"; do
    wget --show-progress -qO "$file" "${files[$file]}"
    sleep 1
    echo "$file"
    if [ $? -ne 0 ]; then
        echo "Failed to download $file"
        exit 1
    fi
done

echo 1 > /proc/sys/vm/drop_caches
echo 2 > /proc/sys/vm/drop_caches
echo 3 > /proc/sys/vm/drop_caches

declare -a scripts=(
    "bein.py"
    "beinConnect.py"
    "beinent.py"
    "beincin.py"
    "beinent.py"
    "beinentC.py"
    "elcin.py"
    "elcinEN.py"
    "uaeariet5.py"
    "uaeeniet5.py"
    "beinsportiet5.py"
)

for script in "${scripts[@]}"; do
    python "/usr/lib/enigma2/python/Plugins/Extensions/EPGGrabber/providers/$script"
    if [ $? -ne 0 ]; then
        echo "Failed to execute $script"
        exit 1
    fi
done




