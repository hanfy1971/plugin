#!/bin/bash
echo ''
echo '************************************************************'
echo "**                         STARTED                        **"
echo '************************************************************'
echo "**                 Uploaded by: Tarekha                   **"
echo "************************************************************"
echo ''
sleep 3s

wget -O /tmp/vfd-display-480-atv-skins-stein17-v1.1.tar.gz "https://gitlab.com/hanfy1971/plugin/-/raw/main/lcdskin/lcdskin/vfd-display-480-atv-skins-stein17-v1.1.tar.gz"

tar -xzf /tmp/*.tar.gz -C /

rm -r /tmp/vfd-display-480-atv-skins-stein17-v1.1.tar.gz

sleep 2;
echo "############ INSTALLATION COMPLETED ########"
echo "############ RESTARTING... #################" 
init 4
sleep 2
init 3
exit 0
