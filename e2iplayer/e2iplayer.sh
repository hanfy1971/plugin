#!/bin/bash
echo ''
echo '************************************************************'
echo "**                         STARTED                        **"
echo '************************************************************'
echo "**                 Uploaded by: Tarekha                   **"
echo "************************************************************"
echo ''
sleep 3s

wget -O /tmp/iptvplayer-3.9.x.tar.gz "https://gitlab.com/hanfy1971/plugin/-/raw/main/e2iplayer/iptvplayer-3.9.x.tar.gz"

tar -xzf /tmp/*.tar.gz -C /

rm -r /tmp/iptvplayer-3.9.x.tar.gz

sleep 2;
echo "############ INSTALLATION COMPLETED ########"
echo "############ RESTARTING... #################" 
init 4
sleep 2
init 3
exit 0
