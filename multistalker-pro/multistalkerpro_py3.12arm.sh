#!/bin/sh
echo ''
echo '************************************************************'
echo "**                         STARTED                        **"
echo '************************************************************'
echo "**                 Uploaded by: TAREK-HANFY                  **"
echo "************************************************************"
echo ''
sleep 1s

wget -O /tmp/multi-stalkerpro_1.1+git44-r0.tar.gz "https://gitlab.com/hanfy1971/plugin/-/raw/main/multistalker-pro/multi-stalkerpro_1.1+git44-r0.tar.gz"

tar -xzf /tmp/*.tar.gz -C /

rm -r /tmp/multi-stalkerpro_1.1+git44-r0.tar.gz

wget -O /tmp/python3-rarfile_4.1-r0.tar.gz "https://gitlab.com/hanfy1971/plugin/-/raw/main/multistalker-pro/multi-stalkerpro_1.1-arm/python3-rarfile_4.1-r0.tar.gz"

tar -xzf /tmp/*.tar.gz -C /

rm -r /tmp/python3-rarfile_4.1-r0.tar.gz

wget -O /tmp/python3-levenshtein_0.12.0-r0.tar.gz "https://gitlab.com/hanfy1971/plugin/-/raw/main/multistalker-pro/multi-stalkerpro_1.1-arm/python3-levenshtein_0.12.0-r0.tar.gz"

tar -xzf /tmp/*.tar.gz -C /

rm -r /tmp/python3-levenshtein_0.12.0-r0.tar.gz

wget -O /tmp/python3-fuzzywuzzy_0.18.0-r0.tar.gz "https://gitlab.com/hanfy1971/plugin/-/raw/main/multistalker-pro/python3-fuzzywuzzy_0.18.0-r0.tar.gz"

tar -xzf /tmp/*.tar.gz -C /

rm -r /tmp/python3-fuzzywuzzy_0.18.0-r0.tar.gz

sleep 2;
echo "############ INSTALLATION COMPLETED ########"
echo "############ RESTARTING... #################" 
init 4
sleep 2
init 3
exit 0
