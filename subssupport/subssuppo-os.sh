#!/bin/sh
#

wget -O /tmp/subssupport_1.5.8-20200522_all.deb
"https://gitlab.com/hanfy1971/plugin/-/main/subssupport/subssupport_1.5.8-20200522_all.deb"

dpkg -i --force-overwrite /tmp/*.deb

rm -r /tmp/subssupport_1.5.8-20200522_all.deb

wait

echo "> Setup the plugin..."
# Configure ajpanel_settings
touch "$temp_dir/temp_file"
cat <<EOF > "$temp_dir/temp_file"
config.plugins.subtitlesSupport.encodingsGroup=Arabic
config.plugins.subtitlesSupport.external.font.size=52
config.plugins.subtitlesSupport.search.edna_cz.enabled=False
config.plugins.subtitlesSupport.search.itasa.enabled=False
config.plugins.subtitlesSupport.search.lang1=ar
config.plugins.subtitlesSupport.search.lang2=ar
config.plugins.subtitlesSupport.search.lang3=ar
config.plugins.subtitlesSupport.search.mysubs.enabled=False
config.plugins.subtitlesSupport.search.opensubtitles.enabled=False
config.plugins.subtitlesSupport.search.podnapisi.enabled=False
config.plugins.subtitlesSupport.search.prijevodionline.enabled=False
config.plugins.subtitlesSupport.search.serialzone_cz.enabled=False
config.plugins.subtitlesSupport.search.subscene.enabled=False
config.plugins.subtitlesSupport.search.subtitles_gr.enabled=False
config.plugins.subtitlesSupport.search.subtitlist.enabled=False
config.plugins.subtitlesSupport.search.titlovi.enabled=False
config.plugins.subtitlesSupport.search.titulky_com.enabled=False
EOF

# Update Enigma2 settings
sed -i "/subtitlesSupport/d" /etc/enigma2/settings
grep "config.plugins.subtitlesSupport.*" "$temp_dir/temp_file" >> /etc/enigma2/settings
rm -rf "$temp_dir/temp_file" >/dev/null 2>&1

sleep 2
echo "> Setup done..., Please wait enigma2 restarting..."
sleep 1
echo "> Uploaded By TAREK"

# Restart Enigma2 service or kill enigma2 based on the system
if [ -f /etc/apt/apt.conf ]; then
    sleep 2
    systemctl restart enigma2
else
    sleep 2
    killall -9 enigma2
fi
else
echo "> $plugin-$version package installation failed"
sleep 3
fi


