#!/bin/bash
echo ''
echo '************************************************************'
echo "**                         STARTED                        **"
echo '************************************************************'
echo "**                 Uploaded by: Tarekha                   **"
echo "************************************************************"
echo ''
sleep 3s

wget -O /tmp/ipaudiopro_1.1+git112+3933b300.tar.gz "https://gitlab.com/hanfy1971/plugin/-/raw/main/ipaudiopro-last/ipaudiopro_1.1+git112+3933b300.tar.gz"

tar -xzf /tmp/*.tar.gz -C /

rm -r /tmp/ipaudiopro_1.1+git112+3933b300.tar.gz

sleep 2;
echo "############ INSTALLATION COMPLETED ########"
echo "############ RESTARTING... #################" 
init 4
sleep 2
init 3
exit 0
